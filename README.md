# SLONY EN WINDOWS

### VERSIONES DE SOFTWARE 

#### Servidor maestro
  - Windows 10 64 bits
  - PostgreSQL 9.5
  - Slony 2.2.5
  - IP : `1.14.24.13`

#### Servidor esclavo
  - Windows 8.1 64 bits
  - PostgreSQL 9.5
  - Slony 2.2.5
  - IP : `1.14.24.14`
  
### CONSIDERACIONES IMPORTANTES
- La codificaci�n de las bases de datos deben ser las mismas (UTF-8, LATIN1, etc).
- La configuraci�n regional y el tiempo deben coincidir.
- Los sistemas operativos de cada servidor pueden variar.
- Las versiones de bases de datos PostgreSQL puenden variar.

### INSTALACI�N

En el caso de que el sistema operativo sea de 64bits y la version de postgres sea 9.5, 
entonces descargar slony en el siguiente enlace [Slony 2.2.5](http://get.enterprisedb.com/stackbuilder/slony-pg95-2.2.5-1-windows-x64.exe) ,
caso contrario debera buscar su instalador en un archivo XML de acuerdo al sistema 
operativo y la version de postgres que utilize, el XML es : [XML de referencia](https://www.postgresql.org/applications-v2.xml)

Una vez descargado el instalador de SLONY, proceder a instalarlo en el servidor maestro y en el esclavo.

#### Primer paso

Para la creaci�n del Slony, se debe de configurar el archivo: `pg_hba.conf` el cual permitir� agregar las direcciones IP donde se realizar� la replicaci�n MAESTRO-ESCLAVO. La ruta del archivo es:

```
C:\Program Files\PostgreSQL\9.5\data\pg_hba.conf
```

Al abrir el archivo deber� agregar las siguientes lineas que hacen referencia a las IP's de 
la maquina maestro como la maquina esclavo.

> `OBS:` esto debe realizarse en ambas maquinas (maestro-esclavo).

```config
#maestro
host    all         all         1.14.24.13/32          md5
#esclavo
host    all         all         1.14.24.14/32          md5
```


#### Segundo paso

En ambos servidores (maestro-esclavo) referenciar el path de SLONY, para ello entrar a `File -> Options`.

![Imagen Path](https://gitlab.com/CrisXuX/slony/blob/master/img/img1.png)



#### Tercer paso

Creamos scripts que permitiran controlar lo que se debe sincronizar, se trata de dos scripts,
uno que debe estar alojado en el servidor maestro y el otro en el servidor esclavo.

Los scripts deben estar situados en la siguiente direcci�n de su respectivo servidor:

```console
C:\Program Files\PostgreSQL\9.5\bin\mestro.txt
```

```console
C:\Program Files\PostgreSQL\9.5\bin\esclavo.txt
```

##### Script del servidor Maestro

El script del servidor maestro debera tener el siguiente c�digo de ejemplo : 

```config
cluster name = slony_prueba;

#SERVIDOR MAESTRO
node 1 admin conninfo = 'dbname		=	pedidos		
						 host		=	1.14.24.13
						 user		= 	postgres
						 password	= 	postgres';

#SERVIDOR ESCLAVO						 
node 2 admin conninfo = 'dbname		=	pedidos
						 host		=	1.14.24.14
						 user		= 	postgres
						 password	= 	postgres';
						 
init cluster (id=1, comment= 'nodo maestro');

create set (id=1, origin=1, comment='Todas las tablas');

#HACEMOS REFERENCIA A LAS TABLAS QUE QUERAMOS QUE SE ENCUENTREN SINCRONIZADAS ENTRE SERVIDORES
set add table (set id=1, origin=1, id=1, fully qualified name='public.categorias', comment='tabla categorias');
set add table (set id=1, origin=1, id=2, fully qualified name='public.clientes', comment='tabla clientes');
set add table (set id=1, origin=1, id=3, fully qualified name='public.detalle_ordenes', comment='tabla detalle_ordenes');
set add table (set id=1, origin=1, id=4, fully qualified name='public.empleados', comment='tabla empleados');
set add table (set id=1, origin=1, id=5, fully qualified name='public.ordenes', comment='tabla ordenes');
set add table (set id=1, origin=1, id=6, fully qualified name='public.productos', comment='tabla productos');
set add table (set id=1, origin=1, id=7, fully qualified name='public.proveedores', comment='tabla proveedores');

store node (id=2, comment = 'Nodo Esclavo', EVENT NODE=1);

store path (server = 1, client = 2, conninfo = 'dbname=pedidos host=1.14.24.13 user=postgres password=postgres');
store path (server = 2, client = 1, conninfo = 'dbname=pedidos host=1.14.24.14 user=postgres password=postgres');
store listen (origin=1, provider = 1, receiver =2);
store listen (origin=2, provider = 2, receiver =1);
```

##### Script del servidor Esclavo

El script del servidor esclavo debera tener el siguiente c�digo de ejemplo :

```config
cluster name = slony_prueba;

#SERVIDOR MAESTRO
node 1 admin conninfo = 'dbname		=	pedidos
						 host		=	1.14.24.13
						 user		= 	postgres
						 password	= 	postgres';
						 
#SERVIDOR ESCLAVO						 
node 2 admin conninfo = 'dbname		=	pedidos
						 host		=	1.14.24.14
						 user		= 	postgres
						 password	= 	postgres';

subscribe set (id=1, provider=1, receiver=2, forward=yes);
```


#### Cuarto paso
Ejecutamos los scripts para que creen un esquema de replica en cada servidor de base de datos.

```console
C:\Program Files\PostgreSQL\9.5\bin\slonik maestro.txt
```
Si el script se encuentra correctamente codificado, no nos mostrara errores en la consola.

Procedemos a ejecutar el script en el servidor esclavo.

```console
C:\Program Files\PostgreSQL\9.5\bin\slonik esclavo.txt
```


#### Quinto paso
Para que la replicaci�n se encuentre en funcionamiento, se debera ejecutar el siguiente comando en ambos servidores para poner
en ejecuci�n el servicio de replicaci�n

```console
C:\Program Files\PostgreSQL\9.5\bin\slon slony_prueba "dbname=pedidos user=postgres password=******"
```
Con todo esto ya deberia estar en funcionamiento la replicaci�n entre bases de datos.
